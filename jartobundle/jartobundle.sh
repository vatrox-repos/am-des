#!/bin/bash

path=`pwd`
jarFileName=$1

jarFileFullName=$path"/"$jarFileName
initFileFull=$path"/init.sh"
jarFileNameBundle=$(echo "${jarFileName//.jar/_1.0.0.jar}")
jarFileFullNameBundle=$path'/'$jarFileNameBundle

echo 'input jar file: '$jarFileName
echo 'output jar file: '$jarFileNameBundle

if [ -f "$jarFileFullNameBundle" ]; then
    echo "the jar file already exists, we delete it"
    rm $jarFileFullNameBundle
fi


printf '#!/bin/bash\n\nbash /home/wso2carbon/wso2am-analytics-2.6.0/bin/jartobundle.sh /tmp/'$jarFileName' /tmp/\nchmod 777 -R /tmp/\n' > init.sh
chmod 777 -R $path/

#echo "docker run -it --rm --env JAVA_HOME=/home/wso2carbon/java --env WSO2_SERVER_HOME=/home/wso2carbon/wso2am-analytics-2.6.0 -v $path:/tmp/ -v $initFileFull:/home/wso2carbon/init.sh docker.wso2.com/wso2am-analytics-worker:2.6.0"
`docker run -it --rm --env JAVA_HOME=/home/wso2carbon/java --env WSO2_SERVER_HOME=/home/wso2carbon/wso2am-analytics-2.6.0 -v $path:/tmp/ -v $initFileFull:/home/wso2carbon/init.sh docker.wso2.com/wso2am-analytics-worker:2.6.0`


rmdir $path/hsperfdata_wso2carbon
rm $initFileFull

cp $jarFileFullNameBundle $jarFileFullNameBundle'_'
rm $jarFileFullNameBundle
mv $jarFileFullNameBundle'_' $jarFileFullNameBundle
